:- ['airlines.pl'].

% Check that Origin can reach Destination
oldreach(Origin, Destination) :-
	flight(_, Origin, Destination, _, _);
	flight(_, Destination, Origin, _, _).

oldreach(Origin, Destination) :-
	flight(_, Origin, Z, _, _),
	oldreach(Z, Destination).

directreach(Origin, Destination, Airline) :-
	flight(Airline, Origin, Destination, _, _);
	flight(Airline, Destination, Origin, _, _).

newreach(Origin, Destination, _, [[Origin, Airline, Destination]]) :-
	directreach(Origin, Destination, Airline).

newreach(Origin, Destination, Visited, Path) :-
	directreach(Origin, Z, Air),
	not(member(Z, Visited)),
	newreach(Z, Destination, [Z|Visited], ZPath),
	Origin \= Destination,
	Destination \= Z,
	Path = [[Origin, Air, Z]|ZPath].

% End


% Check the price of the entire flight
flightprice([Origin, Airline, Destination], Price) :-
	flight(Airline, Origin, Destination, Price, _);
	flight(Airline, Destination, Origin, Price, _).

tax([Origin, _, _], Tax) :-
	airport(Origin, Tax, _).

flightpricem([H|T], Price) :-
	T == [],
	tax(H, Price1),
	flightprice(H, Price2),
	Price is Price1 + Price2.

flightpricem([H|T], Price) :-
	T \= [],
	flightprice(H, Price1),
	flightpricem(T, Price2),
	tax(H, Price3),
	Price is Price1 + Price2 + Price3.

flightpriceinit([H|T], Price) :-
	T == [],
	flightprice(H, Price1),
	tax(H, Price2),
	Price is Price1 + Price2, !.

flightpriceinit([H|T], Price) :-
	T \= [],
	flightprice(H, Price1),
	tax(H, Price2),
	flightpricem(T, Price3),
	Price is Price1 + Price2 + Price3, !.

% End

% Check the duration of the entire flight
flightdurationleg([O, A, D], Duration) :-
	flight(A, O, D, _, Duration);
	flight(A, D, O, _, Duration).

flightdurationsecurity([O, _, _], Duration) :-
	airport(O, _, Duration).

flightdurationm([H|T], Duration) :-
	T == [],
	flightdurationleg(H, Duration1),
	flightdurationsecurity(H, Duration2),
	Duration is Duration1 + Duration2.

flightdurationm([H|T], Duration) :-
	T \= [],
	flightdurationleg(H, Duration1),
	flightdurationm(T, Duration2),
	flightdurationsecurity(H, Duration3),
	Duration is Duration1 + Duration2 + Duration3.

flightdurationinit([], 0).
flightdurationinit([H|T], Duration) :-
	T == [],
	flightdurationleg(H, Duration1),
	flightdurationsecurity(H, Duration2),
	Duration is Duration1 + Duration2, !.

flightdurationinit([H|T], Duration) :-
	T \= [],
	flightdurationleg(H, Duration1),
	flightdurationsecurity(H, Duration2),
	flightdurationm(T, Duration3),
	Duration is Duration1 + Duration2 + Duration3, !.

% End

% Check the number of airlines in the flight

airlinesfrompathm([[_, H2, _]|T], List, Airlines) :-
	T == [],
	Airlines = [H2|List].

airlinesfrompathm([[_, H2, _]|T], List, Airlines) :-
	T \= [],
	airlinesfrompathm(T, [H2|List], Airlines).

airlinesfrompathinit([[_, H2, _]|T], Airlines) :-
	T == [],
	Airlines = [H2].

airlinesfrompathinit([[_, H2, _]|T], Airlines) :-
	T \= [],
	airlinesfrompathm(T, [H2], Airlines).

numair(Path, Num) :-
	airlinesfrompathinit(Path, Air1),
	sort(Air1, SAir1),
	length(SAir1, Num), !.

% End

% trip
trip(Origin, Destination, [Price, Duration, NumAirlines|[Path]]) :-
	newreach(Origin, Destination, [Origin], Path),
	flightpriceinit(Path, Price),
	flightdurationinit(Path, Duration),
	numair(Path, NumAirlines).

% End

% tripk
tripk(Origin, Destination, K, [Price, Duration, NumAirlines|[Path]]) :-
	trip(Origin, Destination, [Price, Duration, NumAirlines|[Path]]),
	Duration < K.

% End

% multicitytrip
multicitytrip(Origin, Destination, Intermediate, [Price, Duration, NumAirlines|[Path]]) :-
	trip(Origin, Destination, [Price, Duration, NumAirlines|[Path]]),
	flatten(Path, FPath),
	delete(FPath, Origin, FPath1),
	delete(FPath1, Destination, FPath2),
	member(Intermediate, FPath2).

% End





